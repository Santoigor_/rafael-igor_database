--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5 (Ubuntu 11.5-1)
-- Dumped by pg_dump version 11.5 (Ubuntu 11.5-1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: aluno; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aluno (
    id integer NOT NULL,
    nome character varying(30),
    periodo character(6),
    email character varying(30),
    monitor integer
);


ALTER TABLE public.aluno OWNER TO postgres;

--
-- Name: aluno_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aluno_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aluno_id_seq OWNER TO postgres;

--
-- Name: aluno_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aluno_id_seq OWNED BY public.aluno.id;


--
-- Name: coordenacao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.coordenacao (
    id integer NOT NULL,
    coordenador character varying(30),
    administrador integer
);


ALTER TABLE public.coordenacao OWNER TO postgres;

--
-- Name: coordenacao_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.coordenacao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.coordenacao_id_seq OWNER TO postgres;

--
-- Name: coordenacao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.coordenacao_id_seq OWNED BY public.coordenacao.id;


--
-- Name: curso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.curso (
    id integer NOT NULL,
    carga_horaria integer,
    nome character varying(30),
    codigo character(3),
    gestor integer
);


ALTER TABLE public.curso OWNER TO postgres;

--
-- Name: curso_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.curso_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.curso_id_seq OWNER TO postgres;

--
-- Name: curso_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.curso_id_seq OWNED BY public.curso.id;


--
-- Name: departamento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.departamento (
    id integer NOT NULL,
    sigla character(3),
    gestor integer
);


ALTER TABLE public.departamento OWNER TO postgres;

--
-- Name: departamento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.departamento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.departamento_id_seq OWNER TO postgres;

--
-- Name: departamento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.departamento_id_seq OWNED BY public.departamento.id;


--
-- Name: funcionarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.funcionarios (
    id integer NOT NULL,
    matricula character(9),
    nome character varying(30),
    email character varying(30),
    admistrador integer
);


ALTER TABLE public.funcionarios OWNER TO postgres;

--
-- Name: funcionarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.funcionarios_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.funcionarios_id_seq OWNER TO postgres;

--
-- Name: funcionarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.funcionarios_id_seq OWNED BY public.funcionarios.id;


--
-- Name: materia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.materia (
    id integer NOT NULL,
    codigo character varying(10),
    assunto character varying(20),
    departamento_resp integer
);


ALTER TABLE public.materia OWNER TO postgres;

--
-- Name: materia_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.materia_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.materia_id_seq OWNER TO postgres;

--
-- Name: materia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.materia_id_seq OWNED BY public.materia.id;


--
-- Name: professor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.professor (
    id integer NOT NULL,
    matricula character(9),
    email character varying(20),
    nome character varying(20),
    departamento_resp integer
);


ALTER TABLE public.professor OWNER TO postgres;

--
-- Name: professor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.professor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.professor_id_seq OWNER TO postgres;

--
-- Name: professor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.professor_id_seq OWNED BY public.professor.id;


--
-- Name: professorministraraluno; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.professorministraraluno (
    id integer NOT NULL,
    professor_id integer,
    aluno_id integer
);


ALTER TABLE public.professorministraraluno OWNER TO postgres;

--
-- Name: professorministraraluno_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.professorministraraluno_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.professorministraraluno_id_seq OWNER TO postgres;

--
-- Name: professorministraraluno_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.professorministraraluno_id_seq OWNED BY public.professorministraraluno.id;


--
-- Name: professorministrarturma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.professorministrarturma (
    id integer NOT NULL,
    professor_id integer,
    turma_id integer
);


ALTER TABLE public.professorministrarturma OWNER TO postgres;

--
-- Name: professorministrarturma_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.professorministrarturma_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.professorministrarturma_id_seq OWNER TO postgres;

--
-- Name: professorministrarturma_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.professorministrarturma_id_seq OWNED BY public.professorministrarturma.id;


--
-- Name: reitoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reitoria (
    id integer NOT NULL,
    reitor character varying(30)
);


ALTER TABLE public.reitoria OWNER TO postgres;

--
-- Name: reitoria_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reitoria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reitoria_id_seq OWNER TO postgres;

--
-- Name: reitoria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reitoria_id_seq OWNED BY public.reitoria.id;


--
-- Name: turma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.turma (
    id integer NOT NULL,
    vagas integer,
    codigo character varying(5) NOT NULL
);


ALTER TABLE public.turma OWNER TO postgres;

--
-- Name: turma_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.turma_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.turma_id_seq OWNER TO postgres;

--
-- Name: turma_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.turma_id_seq OWNED BY public.turma.id;


--
-- Name: aluno id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno ALTER COLUMN id SET DEFAULT nextval('public.aluno_id_seq'::regclass);


--
-- Name: coordenacao id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.coordenacao ALTER COLUMN id SET DEFAULT nextval('public.coordenacao_id_seq'::regclass);


--
-- Name: curso id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.curso ALTER COLUMN id SET DEFAULT nextval('public.curso_id_seq'::regclass);


--
-- Name: departamento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.departamento ALTER COLUMN id SET DEFAULT nextval('public.departamento_id_seq'::regclass);


--
-- Name: funcionarios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.funcionarios ALTER COLUMN id SET DEFAULT nextval('public.funcionarios_id_seq'::regclass);


--
-- Name: materia id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materia ALTER COLUMN id SET DEFAULT nextval('public.materia_id_seq'::regclass);


--
-- Name: professor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professor ALTER COLUMN id SET DEFAULT nextval('public.professor_id_seq'::regclass);


--
-- Name: professorministraraluno id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professorministraraluno ALTER COLUMN id SET DEFAULT nextval('public.professorministraraluno_id_seq'::regclass);


--
-- Name: professorministrarturma id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professorministrarturma ALTER COLUMN id SET DEFAULT nextval('public.professorministrarturma_id_seq'::regclass);


--
-- Name: reitoria id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reitoria ALTER COLUMN id SET DEFAULT nextval('public.reitoria_id_seq'::regclass);


--
-- Name: turma id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turma ALTER COLUMN id SET DEFAULT nextval('public.turma_id_seq'::regclass);


--
-- Data for Name: aluno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aluno (id, nome, periodo, email, monitor) FROM stdin;
1	Renato Gomes	2015.1	renatinho@id.uff.br	\N
2	Rafael Carrilho	2019.1	rafael@id.uff.br	1
\.


--
-- Data for Name: coordenacao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.coordenacao (id, coordenador, administrador) FROM stdin;
1	Aline Paes	1
2	Roberto Martins	1
3	Gustavo Mendonça	1
\.


--
-- Data for Name: curso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.curso (id, carga_horaria, nome, codigo, gestor) FROM stdin;
1	3000	Ciencia da Computacao	172	1
2	2300	Sistemas de INformacao	172	3
\.


--
-- Data for Name: departamento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.departamento (id, sigla, gestor) FROM stdin;
1	GMA	2
2	TCC	1
\.


--
-- Data for Name: funcionarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.funcionarios (id, matricula, nome, email, admistrador) FROM stdin;
\.


--
-- Data for Name: materia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.materia (id, codigo, assunto, departamento_resp) FROM stdin;
1	AGHER	CALCULO II-A	1
2	AG123	PROGRAMAÇÃO I	2
\.


--
-- Data for Name: professor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.professor (id, matricula, email, nome, departamento_resp) FROM stdin;
1	123123123	altair@gmail.br	Altair da Silva	\N
2	321321321	diogo@id.uff.br	Diogo Menezes	\N
\.


--
-- Data for Name: professorministraraluno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.professorministraraluno (id, professor_id, aluno_id) FROM stdin;
1	1	1
2	1	2
3	2	1
\.


--
-- Data for Name: professorministrarturma; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.professorministrarturma (id, professor_id, turma_id) FROM stdin;
1	1	1
\.


--
-- Data for Name: reitoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reitoria (id, reitor) FROM stdin;
1	Jorge Figal
\.


--
-- Data for Name: turma; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.turma (id, vagas, codigo) FROM stdin;
1	80	B1
2	40	D1
3	11	E1
\.


--
-- Name: aluno_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aluno_id_seq', 2, true);


--
-- Name: coordenacao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.coordenacao_id_seq', 3, true);


--
-- Name: curso_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.curso_id_seq', 2, true);


--
-- Name: departamento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.departamento_id_seq', 2, true);


--
-- Name: funcionarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.funcionarios_id_seq', 1, false);


--
-- Name: materia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.materia_id_seq', 2, true);


--
-- Name: professor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.professor_id_seq', 2, true);


--
-- Name: professorministraraluno_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.professorministraraluno_id_seq', 3, true);


--
-- Name: professorministrarturma_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.professorministrarturma_id_seq', 1, true);


--
-- Name: reitoria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reitoria_id_seq', 1, true);


--
-- Name: turma_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.turma_id_seq', 3, true);


--
-- Name: aluno aluno_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno
    ADD CONSTRAINT aluno_pkey PRIMARY KEY (id);


--
-- Name: coordenacao coordenacao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.coordenacao
    ADD CONSTRAINT coordenacao_pkey PRIMARY KEY (id);


--
-- Name: curso curso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.curso
    ADD CONSTRAINT curso_pkey PRIMARY KEY (id);


--
-- Name: departamento departamento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.departamento
    ADD CONSTRAINT departamento_pkey PRIMARY KEY (id);


--
-- Name: funcionarios funcionarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.funcionarios
    ADD CONSTRAINT funcionarios_pkey PRIMARY KEY (id);


--
-- Name: materia materia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materia
    ADD CONSTRAINT materia_pkey PRIMARY KEY (id);


--
-- Name: professor professor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professor
    ADD CONSTRAINT professor_pkey PRIMARY KEY (id);


--
-- Name: professorministraraluno professorministraraluno_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professorministraraluno
    ADD CONSTRAINT professorministraraluno_pkey PRIMARY KEY (id);


--
-- Name: professorministrarturma professorministrarturma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professorministrarturma
    ADD CONSTRAINT professorministrarturma_pkey PRIMARY KEY (id);


--
-- Name: reitoria reitoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reitoria
    ADD CONSTRAINT reitoria_pkey PRIMARY KEY (id);


--
-- Name: turma turma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turma
    ADD CONSTRAINT turma_pkey PRIMARY KEY (id);


--
-- Name: aluno aluno_monitor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno
    ADD CONSTRAINT aluno_monitor_fkey FOREIGN KEY (monitor) REFERENCES public.aluno(id);


--
-- Name: coordenacao coordenacao_administrador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.coordenacao
    ADD CONSTRAINT coordenacao_administrador_fkey FOREIGN KEY (administrador) REFERENCES public.reitoria(id);


--
-- Name: curso curso_gestor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.curso
    ADD CONSTRAINT curso_gestor_fkey FOREIGN KEY (gestor) REFERENCES public.coordenacao(id);


--
-- Name: departamento departamento_gestor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.departamento
    ADD CONSTRAINT departamento_gestor_fkey FOREIGN KEY (gestor) REFERENCES public.coordenacao(id);


--
-- Name: funcionarios funcionarios_admistrador_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.funcionarios
    ADD CONSTRAINT funcionarios_admistrador_fkey FOREIGN KEY (admistrador) REFERENCES public.reitoria(id);


--
-- Name: materia materia_departamento_resp_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.materia
    ADD CONSTRAINT materia_departamento_resp_fkey FOREIGN KEY (departamento_resp) REFERENCES public.departamento(id);


--
-- Name: professor professor_departamento_resp_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professor
    ADD CONSTRAINT professor_departamento_resp_fkey FOREIGN KEY (departamento_resp) REFERENCES public.departamento(id);


--
-- Name: professorministraraluno professorministraraluno_aluno_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professorministraraluno
    ADD CONSTRAINT professorministraraluno_aluno_id_fkey FOREIGN KEY (aluno_id) REFERENCES public.aluno(id);


--
-- Name: professorministraraluno professorministraraluno_professor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professorministraraluno
    ADD CONSTRAINT professorministraraluno_professor_id_fkey FOREIGN KEY (professor_id) REFERENCES public.professor(id);


--
-- Name: professorministrarturma professorministrarturma_professor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professorministrarturma
    ADD CONSTRAINT professorministrarturma_professor_id_fkey FOREIGN KEY (professor_id) REFERENCES public.professor(id);


--
-- Name: professorministrarturma professorministrarturma_turma_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professorministrarturma
    ADD CONSTRAINT professorministrarturma_turma_id_fkey FOREIGN KEY (turma_id) REFERENCES public.turma(id);


--
-- PostgreSQL database dump complete
--

